/*
 * INET        An implementation of the TCP/IP protocol suite for the LINUX
 *             operating system.  INET is implemented using the  BSD Socket
 *             interface as the means of communication with the user level.
 *
 *             Support for INET6 connection oriented protocols.
 *
 * Authors:    See the TCPv6 sources
 *
 *             This program is free software; you can redistribute it and/or
 *             modify it under the terms of the GNU General Public License
 *             as published by the Free Software Foundation; either version
 *             2 of the License, or(at your option) any later version.
 */

#include <linux/module.h>
#include <linux/in6.h>
#include <linux/ipv6.h>
#include <linux/jhash.h>
#include <linux/slab.h>

#include <net/addrconf.h>
#include <net/inet_connection_sock.h>
#include <net/inet_ecn.h>
#include <net/inet_hashtables.h>
#include <net/ip6_route.h>
#include <net/sock.h>
#include <net/inet6_connection_sock.h>
#include <net/sock_reuseport.h>

struct dst_entry *inet6_csk_route_req(const struct sock *sk,
				      struct flowi6 *fl6,
				      const struct request_sock *req,
				      u8 proto)
{
	struct inet_request_sock *ireq = inet_rsk(req);
	const struct ipv6_pinfo *np = inet6_sk(sk);
	struct in6_addr *final_p, final;
	struct dst_entry *dst;

	memset(fl6, 0, sizeof(*fl6));
	fl6->flowi6_proto = proto;
	fl6->daddr = ireq->ir_v6_rmt_addr;
	rcu_read_lock();
	final_p = fl6_update_dst(fl6, rcu_dereference(np->opt), &final);
	rcu_read_unlock();
	fl6->saddr = ireq->ir_v6_loc_addr;
	fl6->flowi6_oif = ireq->ir_iif;
	fl6->flowi6_mark = ireq->ir_mark;
	fl6->fl6_dport = ireq->ir_rmt_port;
	fl6->fl6_sport = htons(ireq->ir_num);
	fl6->flowi6_uid = sk->sk_uid;
	security_req_classify_flow(req, flowi6_to_flowi(fl6));

	dst = ip6_dst_lookup_flow(sk, fl6, final_p);
	if (IS_ERR(dst))
		return NULL;

	return dst;
}
EXPORT_SYMBOL(inet6_csk_route_req);

void inet6_csk_addr2sockaddr(struct sock *sk, struct sockaddr *uaddr)
{
	struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *) uaddr;

	sin6->sin6_family = AF_INET6;
	sin6->sin6_addr = sk->sk_v6_daddr;
	sin6->sin6_port	= inet_sk(sk)->inet_dport;
	/* We do not store received flowlabel for TCP */
	sin6->sin6_flowinfo = 0;
	sin6->sin6_scope_id = ipv6_iface_scope_id(&sin6->sin6_addr,
						  sk->sk_bound_dev_if);
}
EXPORT_SYMBOL_GPL(inet6_csk_addr2sockaddr);

static inline
struct dst_entry *__inet6_csk_dst_check(struct sock *sk, u32 cookie)
{
	return __sk_dst_check(sk, cookie);
}

static struct dst_entry *inet6_csk_route_socket(struct sock *sk,
						struct flowi6 *fl6)
{
	struct inet_sock *inet = inet_sk(sk);
	struct ipv6_pinfo *np = inet6_sk(sk);
	struct in6_addr *final_p, final;
	struct dst_entry *dst;

	memset(fl6, 0, sizeof(*fl6));
	fl6->flowi6_proto = sk->sk_protocol;
	fl6->daddr = sk->sk_v6_daddr;
	fl6->saddr = np->saddr;
	fl6->flowlabel = np->flow_label;
	IP6_ECN_flow_xmit(sk, fl6->flowlabel);
	fl6->flowi6_oif = sk->sk_bound_dev_if;
	fl6->flowi6_mark = sk->sk_mark;
	fl6->fl6_sport = inet->inet_sport;
	fl6->fl6_dport = inet->inet_dport;
	fl6->flowi6_uid = sk->sk_uid;
	security_sk_classify_flow(sk, flowi6_to_flowi(fl6));

	rcu_read_lock();
	final_p = fl6_update_dst(fl6, rcu_dereference(np->opt), &final);
	rcu_read_unlock();

	dst = __inet6_csk_dst_check(sk, np->dst_cookie);
	if (!dst) {
		dst = ip6_dst_lookup_flow(sk, fl6, final_p);

		if (!IS_ERR(dst))
			ip6_dst_store(sk, dst, NULL, NULL);
	}
	return dst;
}







int inet6_csk_xmit(struct sock *sk, struct sk_buff *skb, struct flowi *fl_unused)
{
	struct ipv6_pinfo *np = inet6_sk(sk);
	struct flowi6 fl6;
	struct dst_entry *dst;
	int res;
    
    struct tcp_sock *tp = tcp_sk(sk);
    
    /*
     * FlexECN Block Started
     */
    
    // Creating flexecn headers manually.
    unsigned char *flexecn_dst_buffer = kmalloc(DESTINATION_LENGTH * 8, GFP_ATOMIC);
    unsigned char *flexecn_buffer = kmalloc(HOP_BY_HOP_LENGTH * 8, GFP_ATOMIC);
    
    memset(flexecn_dst_buffer, 0, DESTINATION_LENGTH  * 8);
    memset(flexecn_buffer, 0, HOP_BY_HOP_LENGTH * 8);
    
    
    
    
    
    flexecn_dst_buffer[0] = 6; // TCP is next header 
    flexecn_dst_buffer[1] = DESTINATION_LENGTH - 1; // Length without the first 8 bytes
    
    // Begin of Hop-by-Hop Options, starting with flexecn_request
    flexecn_dst_buffer[2] = FLEXECN_RESPONSE_TLV_TYPE; // Option type
    
    flexecn_dst_buffer[3] = FLEXECN_RESPONSE_TLV_LENGTH;
    flexecn_dst_buffer[4] = FLEXECN_INFORMATION_TYPE_NORMAL; // Requested type
    
    
    flexecn_dst_buffer[5] = 2; // Amount of information packages
    //flexecn_dst_buffer[6] = 0; // resreved
    //flexecn_dst_buffer[7] = 0; // resreved
    
    flexecn_dst_buffer[8] = 1;
    flexecn_dst_buffer[9] = 2;
    flexecn_dst_buffer[10] = 3;
    //flexecn_dst_buffer[11] = 0; // resreved
    
    //flexecn_dst_buffer[12] = 0; // resreved
    //flexecn_dst_buffer[13] = 0; // resreved
    //flexecn_dst_buffer[14] = 0; // resreved
    //flexecn_dst_buffer[15] = 0; // resreved
    
    
    
    memcpy(flexecn_dst_buffer + 16, &tp->info0, sizeof(struct information_option));
    memcpy(flexecn_dst_buffer + 40, &tp->info1, sizeof(struct information_option));
    
    
    
    // Hop by Hop options

    // TODO: Test if it is replaced by the correct header in the renew_option or ipv6_options
    flexecn_buffer[0] = 6; // TCP is next header 
    flexecn_buffer[1] = HOP_BY_HOP_LENGTH - 1; // Length without the first 8 bytes
    
    // Begin of Hop-by-Hop Options, starting with flexecn_request
    flexecn_buffer[2] = FLEXECN_REQUEST_TLV_TYPE; // Option type
    
    flexecn_buffer[3] = FLEXECN_REQUEST_TLV_LENGTH;
    flexecn_buffer[4] = FLEXECN_INFORMATION_TYPE_NORMAL; // Requested type
    //flexecn_buffer[5] = 0	; // Counter for templates
    //flexecn_buffer[6] = 0	; // Padding
    //flexecn_buffer[7] = 0	; // Padding
    
    //------ Informatinon,type option (Header length +1 = 1)
    flexecn_buffer[8] = FLEXECN_INFORMATION_TLV_TYPE;
    flexecn_buffer[9] = FLEXECN_INFORMATION_LENGTH + 2;
    flexecn_buffer[10] = FLEXECN_INFORMATION_TYPE_NORMAL;
    // flexecn_buffer[11] = 0; // Reserved byte
    

    ////---------
    // Second information starting  (Header length +1 = 5)
    //// -------------
    flexecn_buffer[OFFSET_SECOND_INFO + 8] = FLEXECN_INFORMATION_TLV_TYPE;
    flexecn_buffer[OFFSET_SECOND_INFO + 9] = FLEXECN_INFORMATION_LENGTH + 2;
    flexecn_buffer[OFFSET_SECOND_INFO + 10] = FLEXECN_INFORMATION_TYPE_NORMAL;
    //flexecn_buffer[OFFSET_SECOND_INFO + 11] = 0; // Reserved byte

    
    
    // Construction of big buffer
    //memcpy(flexecn_big_buffer, flexecn_buffer, HOP_BY_HOP_LENGTH * 8);
    //memcpy(flexecn_big_buffer + HOP_BY_HOP_LENGTH * 8, flexecn_dst_buffer + 2, DESTINATION_LENGTH * 8 - 2);
    
    //////////////////////////////////////////
    
    unsigned char *flexecn_big_buffer = kmalloc(HOP_BY_HOP_LENGTH * 8 + DESTINATION_LENGTH * 8, GFP_ATOMIC);
    memset(flexecn_big_buffer, 0, HOP_BY_HOP_LENGTH * 8 + DESTINATION_LENGTH * 8);
    
    flexecn_big_buffer[1] = (HOP_BY_HOP_LENGTH + DESTINATION_LENGTH) - 1;
    
    flexecn_big_buffer[2] = FLEXECN_REQUEST_TLV_TYPE; // Option type
    flexecn_big_buffer[3] = FLEXECN_REQUEST_TLV_LENGTH;
    flexecn_big_buffer[4] = FLEXECN_INFORMATION_TYPE_NORMAL; // Requested type
    
    
    flexecn_big_buffer[8] = FLEXECN_INFORMATION_TLV_TYPE;
    flexecn_big_buffer[9] = FLEXECN_INFORMATION_LENGTH + 2;
    flexecn_big_buffer[10] = FLEXECN_INFORMATION_TYPE_NORMAL;
   

    ////---------
    // Second information starting  (Header length +1 = 5)
    //// -------------
    flexecn_big_buffer[OFFSET_SECOND_INFO + 8] = FLEXECN_INFORMATION_TLV_TYPE;
    flexecn_big_buffer[OFFSET_SECOND_INFO + 9] = FLEXECN_INFORMATION_LENGTH + 2;
    flexecn_big_buffer[OFFSET_SECOND_INFO + 10] = FLEXECN_INFORMATION_TYPE_NORMAL;
    
    
    flexecn_big_buffer[OFFSET_DST + 0] =  FLEXECN_RESPONSE_TLV_TYPE;
    flexecn_big_buffer[OFFSET_DST + 1] =  FLEXECN_RESPONSE_TLV_LENGTH;
    flexecn_big_buffer[OFFSET_DST + 2] =  FLEXECN_INFORMATION_TYPE_NORMAL;
    flexecn_big_buffer[OFFSET_DST + 3] =  2;  // Amount of information packages
    
    flexecn_big_buffer[OFFSET_DST + 6] = 1;
    flexecn_big_buffer[OFFSET_DST + 7] = 2;
    flexecn_big_buffer[OFFSET_DST + 8] = 3;
    
    memcpy(flexecn_big_buffer + OFFSET_DST + 14, &tp->info0, sizeof(struct information_option));
    memcpy(flexecn_big_buffer + OFFSET_DST + 38, &tp->info1, sizeof(struct information_option));
    
    
    
    if (1) {
        struct ipv6_txoptions *txopts = NULL;
        struct ipv6_opt_hdr	*hop;
        
        // Add FlexECN Destination Options
        if (0){
            hop = (struct ipv6_opt_hdr *) flexecn_dst_buffer;
        
            txopts = ipv6_renew_options_kern(sk, txopts, IPV6_DSTOPTS, 
                        hop, hop ? ipv6_optlen(hop) : 0);
            
            if (IS_ERR(txopts)) {
                return PTR_ERR(txopts);
            }
        }
        
        // Should be active, if there are two extension headers
        if (0) {
            if (txopts) {
                atomic_sub(txopts->tot_len, &sk->sk_omem_alloc);
            }
        }
        
        // Add Hop by Hop Option
        if (1){
            hop = (struct ipv6_opt_hdr *)  flexecn_big_buffer; //flexecn_buffer; TODO: Switch for two headers instead of one big
            
            txopts = ipv6_renew_options_kern(sk, txopts, IPV6_HOPOPTS,
                hop, hop ? ipv6_optlen(hop) : 0);

            if (IS_ERR(txopts)) {
                return PTR_ERR(txopts);
            }
        }
        
        // Updates socket with atomic swap and updates extension header length for mss adjusting
        txopts = ipv6_update_options(sk, txopts);
        
        if (txopts) {
            atomic_sub(txopts->tot_len, &sk->sk_omem_alloc);
            txopt_put(txopts);
        }
    }

    kfree(flexecn_big_buffer);
    kfree(flexecn_buffer);
    kfree(flexecn_dst_buffer);
    /*
     * FlexECN Block Finished
     */
    
    dst = inet6_csk_route_socket(sk, &fl6);
	if (IS_ERR(dst)) {
		sk->sk_err_soft = -PTR_ERR(dst);
		sk->sk_route_caps = 0;
		kfree_skb(skb);
		return PTR_ERR(dst);
	}
   
    
	rcu_read_lock();
    
    skb_dst_set_noref(skb, dst);

	/* Restore final destination back after routing done */
	fl6.daddr = sk->sk_v6_daddr;
        

	res = ip6_xmit(sk, skb, &fl6, sk->sk_mark, rcu_dereference(np->opt),
		       np->tclass);
    
	rcu_read_unlock();
	return res;
}
EXPORT_SYMBOL_GPL(inet6_csk_xmit);

struct dst_entry *inet6_csk_update_pmtu(struct sock *sk, u32 mtu)
{
	struct flowi6 fl6;
	struct dst_entry *dst = inet6_csk_route_socket(sk, &fl6);

	if (IS_ERR(dst))
		return NULL;
	dst->ops->update_pmtu(dst, sk, NULL, mtu);

	dst = inet6_csk_route_socket(sk, &fl6);
	return IS_ERR(dst) ? NULL : dst;
}
EXPORT_SYMBOL_GPL(inet6_csk_update_pmtu);
