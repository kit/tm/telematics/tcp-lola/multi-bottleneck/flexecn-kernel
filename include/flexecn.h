 
#ifndef _FLEXECN_H
#define _FLEXECN_H


// FlexECN
struct __attribute__((__packed__)) information_option {
	//uint8_t type;
	//uint8_t reserved;

	uint64_t id;
	uint32_t outputrate;
	uint32_t inputrate;
	uint32_t buffersize;
	uint32_t queuesize;
};


// in bytes
#define HOP_BY_HOP_LENGTH 9
#define DESTINATION_LENGTH 8

// FlexECN Request
#define FLEXECN_REQUEST_TLV_TYPE 62
#define FLEXECN_REQUEST_TLV_LENGTH 2

// FlexECN Information
#define FLEXECN_INFORMATION_TLV_TYPE 63
#define FLEXECN_ID_LENGTH 8
#define FLEXECN_STD_LENGTH 4
#define FLEXECN_INFORMATION_LENGTH (FLEXECN_ID_LENGTH + FLEXECN_STD_LENGTH * 4)

// FlexECN Information types
#define FLEXECN_INFORMATION_TYPE_NORMAL 1

#define FLEXECN_RESPONSE_TLV_TYPE 30
#define FLEXECN_RESPONSE_TLV_LENGTH (DESTINATION_LENGTH * 8 -4)


#define OFFSET_SECOND_INFO 32


#define OFFSET_DST (OFFSET_SECOND_INFO + 40)





#endif

    /// first block
    
   
    /*
    *(flexecn_dst_buffer + 16) = tp->info0.id; // id
    *(flexecn_dst_buffer + 24) = tp->info0.outputrate;// // output
    *(flexecn_dst_buffer + 28) = tp->info0.inputrate;// // input
    *(flexecn_dst_buffer + 32) = tp->info0.buffersize; // buffer
    *(flexecn_dst_buffer + 36) = tp->info0.queuesize; // queue
    */
    
    
    /// second block
    /*
    *(flexecn_dst_buffer + 40) = tp->info1.id; // id 
    *(flexecn_dst_buffer + 48) = tp->info1.outputrate; // output
    *(flexecn_dst_buffer + 52) = tp->info1.inputrate; // input
    *(flexecn_dst_buffer + 56) = tp->info1.buffersize; // buffer
    *(flexecn_dst_buffer + 60) = tp->info1.queuesize; // queue
    
    
    printk("id no 0: %llx", tp->info0.id);
    printk("werte 0: %u %u %u %u", tp->info0.outputrate, tp->info0.inputrate, tp->info0.buffersize, tp->info0.queuesize);
    
    
    printk("id no 1: %llx", tp->info1.id);
    printk("werte 1: %u %u %u %u", tp->info1.outputrate, tp->info1.inputrate, tp->info1.buffersize, tp->info1.queuesize);
    */
    

/*


    // ID 
    flexecn_buffer[12] = 0;
    flexecn_buffer[13] = 0;
    flexecn_buffer[14] = 0;
    flexecn_buffer[15] = 0;
    
    // (Header length +1 = 2)
    flexecn_buffer[16] = 0;
    flexecn_buffer[17] = 0;
    flexecn_buffer[18] = 0;
    flexecn_buffer[19] = 0;
    
    // Output rate
    flexecn_buffer[20] = 0;
    flexecn_buffer[21] = 0;
    flexecn_buffer[22] = 0;
    flexecn_buffer[23] = 0;
    
    //input rate (Header length +1 = 3)
    flexecn_buffer[24] = 0;
    flexecn_buffer[25] = 0;
    flexecn_buffer[26] = 0;
    flexecn_buffer[27] = 0;
    
    // Buffer-Size
    flexecn_buffer[28] = 0;
    flexecn_buffer[29] = 0;
    flexecn_buffer[30] = 0;
    flexecn_buffer[31] = 0;
    
    // queue size (Header length +1 = 4)
    flexecn_buffer[32] = 0;
    flexecn_buffer[33] = 0;
    flexecn_buffer[34] = 0;
    flexecn_buffer[35] = 0;
    
    // Padding
    flexecn_buffer[36] = 0;
    flexecn_buffer[37] = 0;
    flexecn_buffer[38] = 0;
    flexecn_buffer[39] = 0;
    */
    









    
    
    /*
    // ID 
    flexecn_buffer[OFFSET_SECOND_INFO + 12] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 13] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 14] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 15] = 0;
    
    // (Header length +1 = 6)
    flexecn_buffer[OFFSET_SECOND_INFO + 16] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 17] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 18] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 19] = 0;
    
    // Output rate
    flexecn_buffer[OFFSET_SECOND_INFO + 20] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 21] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 22] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 23] = 0;
    
    //input rate (Header length +1 = 7)
    flexecn_buffer[OFFSET_SECOND_INFO + 24] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 25] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 26] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 27] = 0;
    
    // Buffer-Size
    flexecn_buffer[OFFSET_SECOND_INFO + 28] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 29] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 30] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 31] = 0;
    
    // queue size (Header length +1 = 8)
    flexecn_buffer[OFFSET_SECOND_INFO + 32] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 33] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 34] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 35] = 0;
    
    // Padding
    flexecn_buffer[OFFSET_SECOND_INFO + 36] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 37] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 38] = 0;
    flexecn_buffer[OFFSET_SECOND_INFO + 39] = 0;
    
    
    */
    
























    
/*


    printk("id no 0: %llx", tp->info0.id);
    printk("werte 0: %u %u %u %u", tp->info0.outputrate, tp->info0.inputrate, tp->info0.buffersize, tp->info0.queuesize);
    
    
    printk("id no 1: %llx", tp->info1.id);
    printk("werte 1: %u %u %u %u", tp->info1.outputrate, tp->info1.inputrate, tp->info1.buffersize, tp->info1.queuesize);





unsigned char *nh = (unsigned char *) &tp->info0;

printk("bita: %u %u %u %u %u %u %u %u", *(nh), *(nh+1),*(nh+2),*(nh+3),*(nh+4),*(nh+5),*(nh+6),*(nh+7) );
printk("bitb: %u %u %u %u %u %u %u %u", *(nh + 8), *(nh+9),*(nh+10),*(nh+11),*(nh+12),*(nh+13),*(nh+14),*(nh+15));

printk("bitc: %u %u %u %u %u %u %u %u", *(nh+ 16), *(nh+17),*(nh+18),*(nh+19),*(nh+20),*(nh+21),*(nh+22),*(nh+23) );
printk("bitd: %u %u %u %u %u %u %u %u", *(nh +24), *(nh+25),*(nh+26),*(nh+27),*(nh+28),*(nh+29),*(nh+30),*(nh+31));

printk("bite: %u %u %u %u %u %u %u %u", *(nh+ 32), *(nh+33),*(nh+34),*(nh+35),*(nh+36),*(nh+37),*(nh+38),*(nh+39) );
printk("bitf: %u %u %u %u %u %u %u %u", *(nh +40), *(nh+41),*(nh+42),*(nh+43),*(nh+44),*(nh+45),*(nh+46),*(nh+47));



printk("id no 0: %llx", tp->info0.id);
printk("out no 0: %u", htons(tp->info0.outputrate));
printk("inp no 0: %u", htons(tp->info0.inputrate));
printk("buf no 0: %u", htons(tp->info0.buffersize));
printk("que no 0: %u", htons(tp->info0.queuesize));

*nh = (unsigned char *) &tp->info1;

printk("bitax: %u %u %u %u %u %u %u %u", *(nh), *(nh+1),*(nh+2),*(nh+3),*(nh+4),*(nh+5),*(nh+6),*(nh+7) );
printk("bitbx: %u %u %u %u %u %u %u %u", *(nh + 8), *(nh+9),*(nh+10),*(nh+11),*(nh+12),*(nh+13),*(nh+14),*(nh+15));

printk("bitcx: %u %u %u %u %u %u %u %u", *(nh+ 16), *(nh+17),*(nh+18),*(nh+19),*(nh+20),*(nh+21),*(nh+22),*(nh+23) );
printk("bitdx: %u %u %u %u %u %u %u %u", *(nh +24), *(nh+25),*(nh+26),*(nh+27),*(nh+28),*(nh+29),*(nh+30),*(nh+31));

printk("bitex: %u %u %u %u %u %u %u %u", *(nh+ 32), *(nh+33),*(nh+34),*(nh+35),*(nh+36),*(nh+37),*(nh+38),*(nh+39) );
printk("bitfx: %u %u %u %u %u %u %u %u", *(nh +40), *(nh+41),*(nh+42),*(nh+43),*(nh+44),*(nh+45),*(nh+46),*(nh+47));

    
    


//printk("skb nach options: %u", skb->len);

//printk("mss_clamp: %u", tp->rx_opt.mss_clamp);




rcu_read_lock();
lock_sock(sk);
txopts = rcu_dereference_protected(np->opt, lockdep_sock_is_held(sk));


txop


ts = rcu_dereference_protected(np->opt, lockdep_sock_is_held(sk));


       
    rcu_read_lock();
    int a =  ipv6_setsockopt(sk, IPPROTO_IPV6, IPV6_DSTOPTS,
        (char __user *) flexecn_dst_buffer, DESTINATION_LENGTH * 8);
    
    rcu_read_unlock();
    
    printk("setsock: %u", a);


{
struct inet_connection_sock *icsk = inet_csk(sk);
icsk->icsk_ext_hdr_len = txopts->opt_flen + txopts->opt_nflen;
}

txopts = xchg((__force struct ipv6_txoptions **)&inet6_sk(sk)->opt, txopts);




release_sock(sk);
rcu_read_unlock();



                
                printk("flowlabel0: %u", flowlabel0);
                printk("flowlabel1: %u", flowlabel1);
                printk("flowlabel2: %u", flowlabel2);
                
                printk("id no 0: %llx", tp->dest_info0.id);
                
                printk("out no 0: %u", tp->dest_info0.outputrate);
                printk("inp no 0: %u", tp->dest_info0.inputrate);
                printk("buf no 0: %u", tp->dest_info0.buffersize);
                printk("que no 0: %u", tp->dest_info0.queuesize);
                 
                printk("id no 1: %llx", tp->dest_info1.id);
                printk("out no 1: %u", tp->dest_info1.outputrate);
                printk("inp no 1: %u", tp->dest_info1.inputrate);
                printk("buf no 1: %u", tp->dest_info1.buffersize);
                printk("que no 1: %u", tp->dest_info1.queuesize);
               
                printk("flexecn_type: %u", flexecn_type);
                printk("flexecn_switch_counter: %u", flexecn_switch_counter);
                
             //printk("bits: %u %u %u %u %u %u %u %u", *(nh), *(nh+1),*(nh+2),*(nh+3),*(nh+4),*(nh+5),*(nh+6),*(nh+7) );
        //printk("bitz: %u %u %u %u %u %u %u %u", *(nh + 8), *(nh+9),*(nh+10),*(nh+11),*(nh+12),*(nh+13),*(nh+14),*(nh+15));
        
       
        printk("bita: %u %u %u %u %u %u %u %u", *(nh), *(nh+1),*(nh+2),*(nh+3),*(nh+4),*(nh+5),*(nh+6),*(nh+7) );
        printk("bitb: %u %u %u %u %u %u %u %u", *(nh + 8), *(nh+9),*(nh+10),*(nh+11),*(nh+12),*(nh+13),*(nh+14),*(nh+15));
        
        printk("bitc: %u %u %u %u %u %u %u %u", *(nh+ 16), *(nh+17),*(nh+18),*(nh+19),*(nh+20),*(nh+21),*(nh+22),*(nh+23) );
        printk("bitd: %u %u %u %u %u %u %u %u", *(nh +24), *(nh+25),*(nh+26),*(nh+27),*(nh+28),*(nh+29),*(nh+30),*(nh+31));
        
        printk("bite: %u %u %u %u %u %u %u %u", *(nh+ 32), *(nh+33),*(nh+34),*(nh+35),*(nh+36),*(nh+37),*(nh+38),*(nh+39) );
        printk("bitf: %u %u %u %u %u %u %u %u", *(nh +40), *(nh+41),*(nh+42),*(nh+43),*(nh+44),*(nh+45),*(nh+46),*(nh+47));
            
                 
                 
                if(0) {
            struct tcp_sock *tp = tcp_sk(sk);
            printk("id no 0: %llx", tp->info0.id);
            printk("werte 0: %u %u %u %u", tp->info0.outputrate, tp->info0.inputrate, tp->info0.buffersize, tp->info0.queuesize);
            
            
            printk("id no 1: %llx", tp->info1.id);
            printk("werte 1: %u %u %u %u", tp->info1.outputrate, tp->info1.inputrate, tp->info1.buffersize, tp->info1.queuesize);
        }
        
        
                printk("id no 0: %llx", tp->info0.id);
                printk("out no 0: %u", tp->info0.outputrate);
                printk("inp no 0: %u", tp->info0.inputrate);
                printk("buf no 0: %u", tp->info0.buffersize);
                printk("que no 0: %u", tp->info0.queuesize);
                
                
                tp->info0.id = 2222;
                printk("id no 1: %llx", tp->info0.id);
                
                
                    printk("id no 1: %llx", tp->info1.id);
                    printk("outputrate: %u", info->outputrate);
                    printk("inputrate: %u", info->inputrate);
                    printk("buffersize: %u", info->buffersize);
                    printk("queuesize: %u", info->queuesize);
                    printk("id no 0: %llx", tp->info0.id);
               
                memset(&tp->info0, 0, sizeof(struct information_option));
                memset(&tp->info1, 0, sizeof(struct information_option));
                memset(&tp->info2, 0, sizeof(struct information_option));
                memset(&tp->info3, 0, sizeof(struct information_option));
*/
